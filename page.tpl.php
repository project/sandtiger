<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
    
<head>

  <title><?php print $head_title ?></title>
  
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <?php require ('includes/block_layout.php');?>
  <?php 
      $font_family = theme_get_setting('font_family');
	  $font_size = theme_get_setting('font_size');
  ?> 	
 
  <!--[if lte IE 6]>
  <script type="text/javascript"> 
    $(document).ready(function(){ 
      $(document).pngFix(); 
    }); 
  </script> 
  <style type="text/css" media="all">@import "<?php echo $base_path ?>sites/all/themes/sandtiger/ie6.css";</style>
  <![endif]-->	
  
<!-- User Controlled Layout -->    
  <?php 
      require ('css/center-align_css.php');
    ?>
  
</head>

<body<?php print phptemplate_body_class($left, $right); ?>>

  <div id="page-wrapper">

<!-- Header -->  
    <div id="header-wrapper" class="clear-block">
      
	  <?php if ($logo): ?>
        <div id="site-logo">
           <img src="<?php print $logo ?>" alt="<?php print $site_name ?>" />
           <?php if ($site_slogan): ?>
           <div id="site-slogan">
	         <h2><?php print $site_slogan ?></h2>
           </div>
          <?php endif; ?>
        </div>
      <?php endif; ?> 
      
	  <?php if ($site_name): ?>
        <div id="site-name">
          <h1><?php print $site_name ?></h1>
          <?php if ($site_slogan): ?>
            <div id="site-slogan">
	          <h2><?php print $site_slogan ?></h2>
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?> 
      
<!-- Search -->    
      <?php if ($search_box): ?>
          <div class="search-box">
            <?php print $search_box ?>
          </div>
      <?php endif; ?> 
   
    </div><!-- /header-wrapper -->
      
<!-- Primary Links -->    
    <?php if ($primary): ?>
	  <div id="primary-links"><div id="primary-links-inner"><div id="primary-links-inner-inner" class="clear-block">
        <?php print $primary ?>
	  </div></div></div>
    <?php endif; ?> 
        
<!-- Main Content -->
    
    <div id="nifty" class="clear-block">
    <b class="rtop"><b class="r1"></b><b class="r2"></b><b class="r3"></b><b class="r4"></b></b>    
    
    <div id="middle-wrapper"> 
      	
      <?php if ($left) { ?>
	    <div class="sidebar-left">
          <?php print $left ?>
	    </div>
      <?php } ?>    	
	 
      <div id="main-content"><div id="squeeze"><div id="main-content-inner">
        <?php print $breadcrumb ?>
        <?php print $tabs ?>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
	    <?php print $feed_icons; ?>
      </div></div></div>
	  
      <?php if ($right) { ?>
	    <div class="sidebar-right">
          <?php print $right ?>
	    </div><!-- /sidebar-right -->
      <?php } ?>    
    
    </div>	
    
    <b class="rbottom"><b class="r4"></b><b class="r3"></b><b class="r2"></b><b class="r1"></b></b>
    </div>

    <div id="nifty-footer">
    <b class="rtop-footer"><b class="r1-footer"></b><b class="r2-footer"></b><b class="r3-footer"></b><b class="r4-footer"></b></b>
    
    <?php if ($secondary) { ?>
      <div id="secondary-links" class="clear-block">
        <?php print $secondary ?>
      </div>
    <?php } ?>
    
    <div style="text-align: center; padding: 10px 0; font-size: 10px; color: #999999;">
	  Design by ThemeShark.com - <a style="color: #ffffff;" href="http://www.themeshark.com/">Drupal Themes with Bite!</a><br /><a href="http://www.themeshark.com/"><img src="http://www.themeshark.com/files/tslogo.png" alt="ThemeShark.com - Drupal Themes with Bite!" /></a>
    </div>
    
	<?php if ($footer) { ?>
	  <div id="footer" class="clear-block">
	    <?php print $footer ?>
	  </div><!-- /footer -->
	<?php } ?>
    
     <b class="rbottom-footer"><b class="r4-footer"></b><b class="r3-footer"></b><b class="r2-footer"></b><b class="r1-footer"></b></b>
     </div>
   
<!-- Script Closure -->
    <?php print $closure ?>
  
  </div><!-- /page-wrapper -->

</body>
</html>
