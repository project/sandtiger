<?php

function phptemplate_settings($saved_settings) {

 $defaults = array(
	'font_family' => 'Arial, Helvetica, sans-serif',
    'font_size' => '0.8',
  );
  
  $settings = array_merge($defaults, $saved_settings);
  
  $form['font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#default_value' => $settings['font_family'],
    '#options' => array (
      'Arial, Helvetica, sans-serif' => t('Arial, Helvetica, Sans-serif'),
      '"Times New Roman", Times, serif' => t('Times New Roman, Times, Serif'),
	  '"Courier New", Courier, monospace' => t('"Courier New", Courier, Monospace'),
	  'Georgia, "Times New Roman", Times, serif' => t('Georgia, "Times New Roman", Times, Serif'),
      'Verdana, Arial, Helvetica, sans-serif' => t('Verdana, Arial, Helvetica, Sans-serif'),
	  'Geneva, Arial, Helvetica, sans-serif' => t('Geneva, Arial, Helvetica, Sans-serif'),
    ),
  );
  
  $form['font_size'] = array(
    '#type' => 'select',
    '#title' => t('Font Size'),
    '#default_value' => $settings['font_size'],
    '#options' => array (
	  '0.7' => t('Small'),
	  '0.8' => t('Default'),
	  '0.9' => t('Large'),  
    ),
  );
  
  return $form;
}