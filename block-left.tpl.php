<div class="sidebox">
 <div class="content">
  <div class="t"></div>
  
  <div class="sidebox-inner">
  
  <?php if ($block->subject): ?>
    <h2 class="title"><span><?php print $block->subject; ?></span></h2>
  <?php endif; ?>

  <div class="sidebox-content">
  <?php print $block->content; ?>
  </div>
  
  </div>
  
 </div>
 <div class="b"><div></div></div>
</div>
