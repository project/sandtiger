<style type="text/css">
body { font-family: <?php echo $font_family ?>; font-size: <?php echo $font_size ?>em; } 
body.two-sidebars #main-content { margin: 0 -200px 0 -200px; }
body.sidebar-left #main-content { margin-left: -200px; }
body.sidebar-right #main-content { margin-right: -200px; }
body.two-sidebars #squeeze { margin: 0 200px 0 200px;}
body.sidebar-left #squeeze { margin-left: 200px; } 
body.sidebar-right #squeeze { margin-right: 200px; }
#middle-wrapper .sidebar-left { width: 200px; }
#middle-wrapper .sidebar-right { width: 200px; }
</style>