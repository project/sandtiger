<?php 

/*Css widths for top boxes*/

$topBlocks = 0;
if ($user1 != '') $topBlocks += 1;
if ($user2 != '') $topBlocks += 1;
if ($user3 != '') $topBlocks += 1;
if ($user4 != '') $topBlocks += 1;

switch ($topBlocks) {
	case 1:
		$topBlocks = "width100";
		break;
	case 2:
		$topBlocks = "width50";
		break;
	case 3:
		$topBlocks = "width33";
		break;
	case 4:
		$topBlocks = "width25";
		break;
	default:
		$topBlocks = "";
}

/*Css widths for bottom boxes*/

$bottomBlocks = 0;
if ($user5 != '') $bottomBlocks += 1;
if ($user6 != '') $bottomBlocks += 1;
if ($user7 != '') $bottomBlocks += 1;
if ($user8 != '') $bottomBlocks += 1;

switch ($bottomBlocks) {
	case 1:
		$bottomBlocks = "width100";
		break;
	case 2:
		$bottomBlocks = "width50";
		break;
	case 3:
		$bottomBlocks = "width33";
		break;
	case 4:
		$bottomBlocks = "width25";
		break;
	default:
		$bottomBlocks = "";
}

?>