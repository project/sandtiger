<?php

/* Body class control */

function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'two-sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

// Initialize Theme Settings

if (is_null(theme_get_setting('font_family'))) {  
  global $theme_key;

  $defaults = array(
	'font_family' => 'Arial, Helvetica, sans-serif',
	'font_size' => '0.8',
  );

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, theme_get_settings($theme_key))
  );

  theme_get_setting('', TRUE);
}

drupal_add_css(drupal_get_path('theme', 'sandtiger') . '/css/default.css', 'theme'); 
drupal_add_js(drupal_get_path('theme', 'sandtiger') . '/js/jquery.pngFix.js', 'theme'); 
